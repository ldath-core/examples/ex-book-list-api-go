[IMPORTANT]
====
This environment is most useful for a developing and testing your application locally.
====

First step is to start services which our application needs to startup properly:

[source,bash]
----
docker-compose -f docker-compose-local.yml up
----

we can also run this in a daemon mode with:

[source,bash]
----
docker-compose -f docker-compose-local.yml up -d
----

Building of our service is done with a help of prepared build script:

[source,bash]
----
./build.sh
----

which looks like this:

[source,shell]
----
include::../build.sh[]
----

this script will generate our service binary.

You can start build service with a help pf prepared serve script:

[source,bash]
----
./serve.sh
----

which looks like this:

[source,shell]
----
include::../serve.sh[]
----

you can confirm with this which port is used by the microservice.

Testing is possible with use of another prepare testing script:

[source,bash]
----
./local-test.sh
----

which looks like this:

[source,shell]
----
include::../local-test.sh[]
----

always run this script prior to pushing to the repository to minify failed pipelines.

Project have also prepared OpenApi files available here:

- `public/v1/openapi.json`
- `public/v1/openapi.yaml`

which allows using tools like by example postmanfootnote:[API platform postman https://www.postman.com/]
