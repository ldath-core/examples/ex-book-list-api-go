#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
./build-multi-stage-container.sh
docker pull postgres:14-alpine
k3d image import -c bookCluster book-list:latest
k3d image import -c bookCluster postgres:14-alpine
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-list ex-book/book-service --version 0.6.1 --dry-run --debug
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-list ex-book/book-service --version 0.6.1 --wait
