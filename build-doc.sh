#!/usr/bin/env bash
cd doc
export APP_NAME=book-list
gem install webrick
gem install bundler
bundle config --local github.https true
bundle config set --local path .bundle/gems && bundle
bundle exec asciidoctor-pdf -a allow-uri-read $APP_NAME.adoc
bundle exec asciidoctor $APP_NAME.adoc
