package config

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func getTestConfig() (*Config, error) {
	cfgFile := os.Getenv("CFG_FILE")
	viper.SetConfigFile(cfgFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return LoadConfig("text")
}

func GetTestSetup(t *testing.T, ctx context.Context) (l *logrus.Logger, d *sql.DB, c *Config) {
	var err error
	// Logger
	l = logrus.New()

	// Config
	c, e := getTestConfig()
	if e != nil {
		t.Fatalf("Error: %s", e.Error())
	}

	// Postgres
	d, err = sql.Open("pgx", c.Postgres.Url)
	if err != nil {
		t.Fatalf("Failed to connect to the Postres, error: %s", err)
	}

	return l, d, c
}
