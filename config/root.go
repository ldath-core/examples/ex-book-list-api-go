package config

import (
	"errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"strings"
	"time"
)

type LoggerConfig struct {
	Level string `mapstructure:"level"`
}

type PostgresConfig struct {
	Url string `mapstructure:"url"`
}

type ServerConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type SentryConfig struct {
	Dsn string `mapstructure:"dsn"`
}

type Config struct {
	Environment string         `mapstructure:"env"`
	Server      ServerConfig   `mapstructure:"server"`
	Logger      LoggerConfig   `mapstructure:"logger"`
	Sentry      SentryConfig   `mapstructure:"sentry"`
	Postgres    PostgresConfig `mapstructure:"postgres"`
}

func LoadConfig(format string) (*Config, error) {
	var config *Config

	logrus.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
		//logrus.SetReportCaller(true)
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	if err := viper.Unmarshal(&config); err != nil {
		logrus.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Errorln("Broken config file")
		return config, err
	}
	if config.Logger.Level == "" {
		logrus.Errorln("Missing config file")
		time.Sleep(5 * time.Minute)
		return config, errors.New("missing config file")
	}
	return config, nil
}
