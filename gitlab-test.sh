#!/usr/bin/env bash
set -e

echo "Execute UT"
CFG_FILE="$(pwd)/secrets/ci.env.yaml" go test -v ./...
