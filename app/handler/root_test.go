package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/db"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/config"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"
)

type TestBookRepository struct {
	BooksRepo         model.BooksRepository
	countBooksFailure bool
	getBooksFailure   bool
	getBookFailure    bool
	addBookFailure    bool
	updateBookFailure bool
	removeBookFailure bool
	pingFailure       bool
}

func NewTestBookRepository(BooksRepo model.BooksRepository) *TestBookRepository {
	return &TestBookRepository{
		BooksRepo:         BooksRepo,
		countBooksFailure: false,
		getBooksFailure:   false,
		getBookFailure:    false,
		addBookFailure:    false,
		updateBookFailure: false,
		removeBookFailure: false,
		pingFailure:       false,
	}
}

func (r *TestBookRepository) CountBooks() (int64, error) {
	if !r.countBooksFailure {
		return r.BooksRepo.CountBooks()
	}
	return 0, errors.New("CountBooks failure from tests")
}

func (r *TestBookRepository) GetBooks(limit, offset int64) ([]model.Book, error) {
	if !r.getBooksFailure {
		return r.BooksRepo.GetBooks(limit, offset)
	}
	return nil, errors.New("GetBooks failure from tests")
}

func (r *TestBookRepository) GetBook(id int64) (*model.Book, error) {
	if !r.getBookFailure {
		return r.BooksRepo.GetBook(id)
	}
	return nil, errors.New("GetBook failure from tests")
}

func (r *TestBookRepository) AddBook(book model.Book) (int, error) {
	if !r.addBookFailure {
		return r.BooksRepo.AddBook(book)
	}
	return 0, errors.New("AddBook failure from tests")
}

func (r *TestBookRepository) UpdateBook(book model.Book) (int64, error) {
	if !r.updateBookFailure {
		return r.BooksRepo.UpdateBook(book)
	}
	return 0, errors.New("UpdateBook failure from tests")
}

func (r *TestBookRepository) RemoveBook(id int) (int64, error) {
	if !r.removeBookFailure {
		return r.BooksRepo.RemoveBook(id)
	}
	return 0, errors.New("RemoveBook failure from tests")
}

func (r *TestBookRepository) Ping() error {
	if !r.pingFailure {
		return r.BooksRepo.Ping()
	}
	return errors.New("Ping failure from tests")
}

const succeed = "\u2713"
const failed = "\u2717"

type CreateTestData struct {
	DropCollection bool
	LoadTestData   bool
}

func createNewRecorder(r *mux.Router, method, endpoint string, qData map[string]string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, endpoint, body)
	if len(qData) >= 0 {
		q := req.URL.Query()
		for k, v := range qData {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	rr := httptest.NewRecorder()
	r.ServeHTTP(rr, req)
	return rr
}

func setup(t *testing.T, ctx context.Context, createTestData CreateTestData) Controller {
	l, d, c := config.GetTestSetup(t, ctx)
	BooksRepo := NewTestBookRepository(model.NewPostgresBookRepository(d))
	controller := Controller{
		BooksRepo: BooksRepo,
		Logger:    l,
		Config:    c,
	}

	if createTestData.DropCollection {
		// Drop all prior to create test data
		err := db.DropAllTables(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}

		// Migrate Database to the latest version
		err = db.MigrateDatabase(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}
	}

	if createTestData.LoadTestData && createTestData.DropCollection {
		// Load Test DATA
		err := db.CreateTestData(ctx, d, l)
		if err != nil {
			t.Fatal(err)
		}
	}
	return controller
}

func testResponseStatusCheck(t *testing.T, expectedStatusCode int, rr *httptest.ResponseRecorder) {
	if status := rr.Code; status != expectedStatusCode {
		t.Errorf("%s check StatusCreated is failed: got %d want %d", failed, status, expectedStatusCode)
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("%s check status %d is successful.", succeed, expectedStatusCode)
	}
}

func testResponseBodyCheck(
	t *testing.T,
	body *bytes.Buffer,
	expectedStatus int,
	expectedMessage string,
	expectedContent *model.Book) {

	success := true

	var response model.Response
	err := json.NewDecoder(body).Decode(&response)
	if err != nil {
		t.Errorf("%s response parsing failed: %s", failed, err)
		success = false
	}

	if response.Status != expectedStatus {
		t.Errorf("%s expected status %d but got %d", failed, expectedStatus, response.Status)
		success = false
	}
	if response.Message != expectedMessage {
		t.Errorf("%s expected message '%s' but got '%s'", failed, expectedMessage, response.Message)
		success = false
	}
	if response.Content == nil {
		t.Errorf("%s expected content but got nil", failed)
		success = false
	}

	content, ok := response.Content.(map[string]interface{})
	if !ok {
		t.Errorf("%s unexpected content type", failed)
		success = false
	}

	id := int(content["id"].(float64))
	if id != expectedContent.ID {
		t.Errorf("%s expected id %d but got %d", failed, expectedContent.ID, id)
		success = false
	}
	if content["title"] != expectedContent.Title {
		t.Errorf("%s expected title '%s' but got '%s'", failed, expectedContent.Title, content["title"])
		success = false
	}
	if content["author"] != expectedContent.Author {
		t.Errorf("%s expected author '%s' but got '%s'", failed, expectedContent.Author, content["author"])
		success = false
	}
	if content["year"] != expectedContent.Year {
		t.Errorf("%s expected year '%s' but got '%s'", failed, expectedContent.Year, content["year"])
		success = false
	}
	if content["description"] != expectedContent.Description {
		t.Errorf("%s expected description '%s' but got '%s'", failed, expectedContent.Description, content["description"])
		success = false
	}

	image, ok := content["image"].(map[string]interface{})
	if !ok {
		t.Errorf("%s unexpected image type", failed)
		success = false
	}

	if expectedContent.Img.Type != "" {
		if image["type"] != expectedContent.Img.Type {
			t.Errorf("%s expected image type '%s' but got '%s'", failed, expectedContent.Img.Type, image["type"])
			success = false
		}
		if image["data"] != expectedContent.Img.Data {
			t.Errorf("%s expected image data '%s' but got '%s'", failed, expectedContent.Img.Data, image["data"])
			success = false
		}
	} else {
		if image["type"] != nil && image["type"] != "" {
			t.Errorf("%s unexpected image type: '%s'", failed, image["type"])
			success = false
		}
		if image["data"] != nil && image["data"] != "" {
			t.Errorf("%s unexpected image data: '%s'", failed, image["data"])
			success = false
		}
	}

	if success {
		t.Logf("%s check response body is successful.", succeed)
	}
}

func checkBookData(t *testing.T, expectedBookData *model.Book) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: false, LoadTestData: false})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.GetBook)

	rr := createNewRecorder(r, "GET", "/v1/books/"+strconv.Itoa(expectedBookData.ID), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)
	testResponseBodyCheck(t, rr.Body, http.StatusOK, "book", expectedBookData)
}
