package handler

import (
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v4"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
)

// CreateBook will handle the create Book post request
func (c *Controller) CreateBook(res http.ResponseWriter, req *http.Request) {
	var book model.Book
	var createdBook model.CreatedBook
	err := json.NewDecoder(req.Body).Decode(&book)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "body json request have issues!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	err = validateBook(&book)
	if err != nil {
		err := ResponseWriter(res, http.StatusNotAcceptable, err.Error(), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	createdBook.ID, err = c.BooksRepo.AddBook(book)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with adding book", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = ResponseWriter(res, http.StatusCreated, fmt.Sprintf("book: %d created", createdBook.ID), createdBook)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetBooks will handle people list get request
func (c *Controller) GetBooks(res http.ResponseWriter, req *http.Request) {
	var bookList []model.Book
	skipString := req.FormValue("skip")
	skip, err := strconv.ParseInt(skipString, 10, 64)
	if err != nil {
		skip = 0
	}
	limitString := req.FormValue("limit")
	limit, err := strconv.ParseInt(limitString, 10, 64)
	if err != nil {
		limit = 10
	}

	if skip < 0 {
		err = ResponseWriter(res, http.StatusNotAcceptable, "Skip value must be non-negative integer", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	if limit < 1 {
		err = ResponseWriter(res, http.StatusNotAcceptable, "Limit value must be positive integer", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	count, err := c.BooksRepo.CountBooks()
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with counting books", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	bookList, err = c.BooksRepo.GetBooks(limit, skip)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with getting books", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	err = PaginatedResponseWriter(res, http.StatusOK, count, skip, limit, fmt.Sprintf("books - skip: %d; limit: %d", skip, limit), bookList)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetBook will give us book with special id
func (c *Controller) GetBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	book, err := c.BooksRepo.GetBook(int64(id))
	if err != nil {
		if err == pgx.ErrNoRows {
			err = ResponseWriter(res, http.StatusNotFound, "there is no document with this id", nil)
		} else {
			err = ResponseWriter(res, http.StatusInternalServerError, "there is an error on server!!!", nil)
		}
		c.Logger.Error(err)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = ResponseWriter(res, http.StatusOK, "book", book)
	if err != nil {
		c.Logger.Error(err)
	}
}

// UpdateBook will handle the book update endpoint
func (c *Controller) UpdateBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	var book model.Book

	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	err = json.NewDecoder(req.Body).Decode(&book)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "body json request have issues!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	book.ID = id

	rowsUpdated, err := c.BooksRepo.UpdateBook(book)
	if err != nil {
		var code int
		var message string
		if err.Error() == "no field for update found" {
			code = http.StatusNotAcceptable
			message = err.Error()
		} else {
			code = http.StatusInternalServerError
			message = "Problem with updating book"
			c.Logger.Error(err)
		}

		err = ResponseWriter(res, code, message, nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	if rowsUpdated == 1 {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("book: %d updated", book.ID), &book)
	} else {
		err = ResponseWriter(res, http.StatusNotFound, "book not found", nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}

func (c *Controller) DeleteBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)

	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	rowsDeleted, err := c.BooksRepo.RemoveBook(id)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with updating book", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if rowsDeleted == 0 {
		err = ResponseWriter(res, http.StatusNotFound, "book not found", nil)
	} else {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("book: %d deleted", id), nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}

func validateBook(book *model.Book) error {
	if book.Author == "" || book.Title == "" || book.Year == "" {
		return fmt.Errorf("please fill all the fields")
	}
	if book.Img.Type == "" && book.Img.Data != "" {
		return fmt.Errorf("image type is required if image data is set")
	}
	if book.Img.Type != "" && book.Img.Data == "" {
		return fmt.Errorf("image data is required if image type is set")
	}
	if book.Img.Data != "" && book.Img.Type != "url" && book.Img.Type != "base64" {
		return fmt.Errorf("image type must be one of: url, base64")
	}
	return nil
}
