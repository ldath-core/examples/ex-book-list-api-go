package handler

import (
	"fmt"
	"github.com/getsentry/sentry-go"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
	"net/http"
)

// more sentry examples: https://github.com/getsentry/sentry-go/blob/master/_examples/http/main.go

// GetHealth will handle health get request
func (c *Controller) GetHealth(res http.ResponseWriter, _ *http.Request) {
	// Ping the primary
	p := true
	if err := c.BooksRepo.Ping(); err != nil {
		p = false
	}

	err := ResponseWriter(res, http.StatusOK, "book-list api health", model.Health{
		Alive:    true,
		Postgres: p,
	})
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetError will handle error get request
func (c *Controller) GetError(w http.ResponseWriter, r *http.Request) {
	// Use GetHubFromContext to get a hub associated with the
	// current request. Hubs provide data isolation, such that tags,
	// breadcrumbs and other attributes are never mixed up across
	// requests.
	ctx := r.Context()
	hub := sentry.GetHubFromContext(ctx)

	hub.Scope().SetTag("url", r.URL.Path)
	hub.CaptureMessage("Error Test message")
	errors := make([]string, 0)
	errors = append(errors, "Error Test message")

	err := ErrorResponseWriter(w, http.StatusOK, "book-list api health", errors)
	if err != nil {
		c.Logger.Error(err)
	}
}

func (c *Controller) GetPanic(w http.ResponseWriter, r *http.Request) {
	var s []int
	_, err := fmt.Fprint(w, s[42]) // this line will panic
	if err != nil {
		c.Logger.Error(err)
	}
}
