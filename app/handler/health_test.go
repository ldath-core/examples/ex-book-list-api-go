package handler

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/gorilla/mux"
)

func TestGetHealth(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{})

	r := mux.NewRouter()
	r.HandleFunc("/v1/health", c.GetHealth)

	rr := createNewRecorder(r, "GET", "/v1/health", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)
}
