package handler

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/config"
)

type Controller struct {
	Logger    *logrus.Logger
	Config    *config.Config
	BooksRepo model.BooksRepository
}

func (c *Controller) NotFound(res http.ResponseWriter, req *http.Request) {
	c.Logger.Warningf("Not found: %s", req.RequestURI)
	err := ResponseWriter(res, http.StatusNotFound, "Page Not Found", nil)
	if err != nil {
		c.Logger.Error(err)
	}
}
