package handler

import (
	sentryhttp "github.com/getsentry/sentry-go/http"
	"net/http"
)

// JSONContentTypeMiddleware will add the json content type header for all endpoints
func JSONContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "application/json; charset=UTF-8")
		next.ServeHTTP(w, r)
	})
}

// SentryMiddleware will add sentry handling for all endpoints
func SentryMiddleware(next http.Handler) http.Handler {
	//handler := sentryhttp.New(sentryhttp.Options{}).Handle(http.DefaultServeMux)
	sentryHandler := sentryhttp.New(sentryhttp.Options{})
	return sentryHandler.Handle(next)
}
