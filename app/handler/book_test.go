package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/jackc/pgx/v4"

	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
)

func TestCreateBookWithoutImage(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:  "Test Book Title",
		Author: "Test Author",
		Year:   "2022"})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)
}

func TestCreateBookWithImageUrl(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "url",
			Data: "test_url",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)
}

func TestCreateBookWithImageBase64(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "base64",
			Data: "aW1hZ2U",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)
}

func TestCreateBookWithEmptyImageType(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "",
			Data: "aW1hZ2U",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusNotAcceptable, rr)
}

func TestCreateBookWithEmptyImageData(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "url",
			Data: "",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusNotAcceptable, rr)
}

func TestCreateBookWithInvalidImageType(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "invalid",
			Data: "aW1hZ2U",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusNotAcceptable, rr)
}

func TestCreateBook_MissingTitle(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})
	e, _ := json.Marshal(model.Book{
		Title:       "",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "url",
			Data: "test_url",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusNotAcceptable, rr)
}

func TestCreateBook_RepoFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{
		DropCollection: true,
		LoadTestData:   false,
	})

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.addBookFailure = true

	e, _ := json.Marshal(model.Book{
		Title:       "Test Book Title",
		Author:      "Test Author",
		Year:        "2022",
		Description: "Test description",
		Img: model.Image{
			Type: "url",
			Data: "test_url",
		},
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func TestGetBooks(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.GetBooks)

	rr := createNewRecorder(r, "GET", "/v1/books", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	rr = createNewRecorder(r, "GET", "/v1/books", map[string]string{"page": "0"}, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

}
func TestGetBooks_RepoCountBooksFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.countBooksFailure = true

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.GetBooks)

	rr := createNewRecorder(r, "GET", "/v1/books", nil, nil)
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func TestGetBooks_RepoFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.getBooksFailure = true

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.GetBooks)

	rr := createNewRecorder(r, "GET", "/v1/books", map[string]string{"page": "0"}, nil)
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func TestGetBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.GetBook)

	id := 1

	rr := createNewRecorder(r, "GET", fmt.Sprintf("/v1/books/%d", id), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	expectedContent := model.Book{
		ID:          1,
		Title:       "Golang is great",
		Author:      "Mr. Great",
		Year:        "2012",
		Description: "Another golang book",
		Img: model.Image{
			Type: "url",
			Data: "https://go.dev/blog/go-brand/logos.jpg"}}

	testResponseBodyCheck(t, rr.Body, http.StatusOK, "book", &expectedContent)
}

func TestGetBookWithoutImage(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.GetBook)

	id := 4

	rr := createNewRecorder(r, "GET", fmt.Sprintf("/v1/books/%d", id), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	expectedContent := model.Book{
		ID:     4,
		Title:  "A book without image",
		Author: "Mr. Image",
		Year:   "2023"}

	testResponseBodyCheck(t, rr.Body, http.StatusOK, "book", &expectedContent)
}

func TestGetBook_RepoFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.getBookFailure = true

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.GetBook)

	id := 1

	rr := createNewRecorder(r, "GET", fmt.Sprintf("/v1/books/%d", id), nil, nil)
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func TestUpdateBookWithEmptyBody(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusNotAcceptable, responseRecorder)
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookTitle(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Title: "Updated Title"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Title = patch.Title
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookAuthor(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Author: "Updated Author"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Author = patch.Author
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookYear(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Year: "2023"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Year = patch.Year
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookDescription(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Description: "Updated description"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Description = patch.Description
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookImgUrl(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{
		Img: model.Image{
			Type: "url",
			Data: "https://updated.com"}}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Img.Type = patch.Img.Type
	bookBeforeUpdate.Img.Data = patch.Img.Data
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookImgBase64(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{
		Img: model.Image{
			Type: "base64",
			Data: "dXBkYXRlZA"}}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	bookBeforeUpdate.Img.Type = patch.Img.Type
	bookBeforeUpdate.Img.Data = patch.Img.Data
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateAllBookFields(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{
		Title:       "Updated title",
		Author:      "Updated Author",
		Year:        "2023",
		Description: "Updated description",
		Img: model.Image{
			Type: "url",
			Data: "https://updated.com"}}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusAccepted, responseRecorder)

	patch.ID = bookBeforeUpdate.ID
	checkBookData(t, &patch)
}

func TestUpdateBookTitleInvalidValueType(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := map[string]int{"title": 0}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusBadRequest, responseRecorder)
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookUnexpectedField(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := map[string]string{"unexpected": "field"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusNotAcceptable, responseRecorder)
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookEmptyTitle(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Title: ""}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusNotAcceptable, responseRecorder)
	checkBookData(t, bookBeforeUpdate)
}

func TestUpdateBookNotFound(t *testing.T) {
	// given
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	router, bookBeforeUpdate, err := setupTestForUpdate(t, ctx)
	if err != nil {
		return
	}

	patch := model.Book{Title: "Updated title"}
	marshaledPatch, _ := json.Marshal(patch)

	// when
	responseRecorder := createNewRecorder(
		router,
		"PUT",
		"/v1/books/"+strconv.Itoa(bookBeforeUpdate.ID+10),
		nil,
		bytes.NewBuffer(marshaledPatch))

	// then
	testResponseStatusCheck(t, http.StatusNotFound, responseRecorder)
	checkBookData(t, bookBeforeUpdate)
}

func TestCheckCountField(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.GetBooks)

	rr := createNewRecorder(r, "GET", "/v1/books", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	body, err := io.ReadAll(rr.Body)
	if err != nil {
		t.Error(err)
	}

	data := model.Response{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		t.Errorf(fmt.Sprintf("Error: %s. Wrong body: %s", err, string(body)))
	}
	field, ok := data.Content.(*model.PaginatedContent)

	// The hardcoded 3 value is describing amout of test data defined in app/db/load.go file.
	if ok && field.Count != 3 {
		t.Errorf(fmt.Sprintf("Wrong number of books. Expected %d, received %d, \n body: %s", 1, 0, string(body)))
	}

}

func TestUpdateBook_InvalidJson(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.UpdateBook)

	book, err := c.BooksRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}
	book.Title = "Updated Title"
	book.Author = "Updated Author"

	e, _ := json.Marshal(book)
	// replace '{' (0x7B) with other value to make buffer invalid
	e[0] = 1
	rr := createNewRecorder(r, "PUT", fmt.Sprintf("/v1/books/%d", book.ID), nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)
}

func TestUpdateBook_RepoFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.UpdateBook)

	book, err := c.BooksRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}
	book.Title = "Updated Title"
	book.Author = "Updated Author"

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.updateBookFailure = true

	e, _ := json.Marshal(book)
	rr := createNewRecorder(r, "PUT", fmt.Sprintf("/v1/books/%d", book.ID), nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func TestDeleteBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.DeleteBook)

	book, err := c.BooksRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr := createNewRecorder(r, "DELETE", fmt.Sprintf("/v1/books/%d", book.ID), nil, nil)
	testResponseStatusCheck(t, http.StatusAccepted, rr)
}

func TestDeleteBook_RepoFailure(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.DeleteBook)

	book, err := c.BooksRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	tr := c.BooksRepo.(*TestBookRepository)
	if tr == nil {
		t.Error("TestBookRepository casting failure, something went wrong with tests initialization")
	}
	tr.removeBookFailure = true

	rr := createNewRecorder(r, "DELETE", fmt.Sprintf("/v1/books/%d", book.ID), nil, nil)
	testResponseStatusCheck(t, http.StatusInternalServerError, rr)
}

func setupTestForUpdate(t *testing.T, ctx context.Context) (*mux.Router, *model.Book, error) {
	controller := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	router := mux.NewRouter()
	router.HandleFunc("/v1/books/{id}", controller.UpdateBook)

	bookBeforeUpdate, err := controller.BooksRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return nil, nil, fmt.Errorf("there is 0 documents in the collection - fix tests")
		}
		t.Error(err)
		return nil, nil, err
	}

	return router, bookBeforeUpdate, nil
}
