package model

type Health struct {
	Alive    bool `json:"alive"`
	Postgres bool `json:"postgres"`
}
