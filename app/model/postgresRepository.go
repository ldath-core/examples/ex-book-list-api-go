package model

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"
)

const dbTimeout = time.Second * 3

type PostgresBookRepository struct {
	PDB *sql.DB
}

func NewPostgresBookRepository(db *sql.DB) *PostgresBookRepository {
	return &PostgresBookRepository{
		PDB: db,
	}
}

func (r *PostgresBookRepository) CountBooks() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	rows, err := r.PDB.QueryContext(ctx, "SELECT COUNT(*) as count FROM books")
	if err != nil {
		return 0, err
	}

	var count int64
	for rows.Next() {
		if err = rows.Scan(&count); err != nil {
			return 0, err
		}
	}

	return count, nil
}

func (r *PostgresBookRepository) GetBooks(limit, offset int64) ([]Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	rows, err := r.PDB.QueryContext(ctx, "SELECT * FROM books LIMIT $1 OFFSET $2", limit, offset)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var books []Book

	for rows.Next() {
		var book Book
		err = rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year, &book.Description, &book.Img.Type, &book.Img.Data)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}

	return books, nil
}

func (r *PostgresBookRepository) GetBook(id int64) (*Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	rows := r.PDB.QueryRowContext(ctx, "SELECT * FROM books WHERE id=$1", id)

	var book Book
	err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year, &book.Description, &book.Img.Type, &book.Img.Data)
	if err != nil {
		return nil, err
	}
	return &book, nil
}

func (r *PostgresBookRepository) AddBook(book Book) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	var bookID int
	err := r.PDB.QueryRowContext(ctx, "INSERT INTO books (title, author, year, description, img_type, img_data) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;",
		book.Title, book.Author, book.Year, book.Description, book.Img.Type, book.Img.Data).Scan(&bookID)

	if err != nil {
		return 0, err
	}

	return bookID, nil
}

func (r *PostgresBookRepository) UpdateBook(patch Book) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	var query strings.Builder
	var fields []string

	query.WriteString("UPDATE books SET ")

	if patch.Title != "" {
		fields = append(fields, fmt.Sprintf("title='%s'", patch.Title))
	}
	if patch.Author != "" {
		fields = append(fields, fmt.Sprintf("author='%s'", patch.Author))
	}
	if patch.Year != "" {
		fields = append(fields, fmt.Sprintf("year='%s'", patch.Year))
	}
	if patch.Description != "" {
		fields = append(fields, fmt.Sprintf("description='%s'", patch.Description))
	}
	if patch.Img.Type != "" {
		fields = append(fields, fmt.Sprintf("img_type='%s'", patch.Img.Type))
	}
	if patch.Img.Data != "" {
		fields = append(fields, fmt.Sprintf("img_data='%s'", patch.Img.Data))
	}

	if len(fields) == 0 {
		return 0, fmt.Errorf("no field for update found")
	}

	query.WriteString(strings.Join(fields, ", "))
	query.WriteString(" WHERE id=$1 RETURNING id")

	result, err := r.PDB.ExecContext(ctx, query.String(), &patch.ID)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}

func (r *PostgresBookRepository) RemoveBook(id int) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()
	result, err := r.PDB.ExecContext(ctx, "DELETE FROM books WHERE id=$1", id)

	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}

func (r *PostgresBookRepository) Ping() error {
	return r.PDB.Ping()
}
