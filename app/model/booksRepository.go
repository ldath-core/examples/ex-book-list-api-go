package model

type BooksRepository interface {
	CountBooks() (int64, error)
	GetBooks(limit, offset int64) ([]Book, error)
	GetBook(id int64) (*Book, error)
	AddBook(book Book) (int, error)
	UpdateBook(book Book) (int64, error)
	RemoveBook(id int) (int64, error)
	Ping() error
}
