package app

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/go-redis/redis/v8"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	_ "github.com/jackc/pgx/v4"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/db"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/handler"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/model"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/routes"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/config"
)

// App has the postgres database and router instances
type App struct {
	Routes    *routes.Config
	RDB       *redis.Client
	Logger    *logrus.Logger
	Config    *config.Config
	BooksRepo model.BooksRepository
	DB        *sql.DB
}

// ConfigAndRunApp will create and initialize App structure. App factory functions.
func ConfigAndRunApp(config *config.Config, logFormat string, migrate, load bool, wait int, corsEnabled bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(config, logFormat, wait)

	if load && migrate {
		// Drop all prior to create test data
		err := db.DropAllTables(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	if migrate {
		// Migrate Database to the latest version
		err := db.MigrateDatabase(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	if load && migrate {
		// Create Test DATA
		err := db.CreateTestData(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	app.Run(fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port), corsEnabled)
}

// ConfigAndLoadTestData will configure and initialize core App structure and then only Load Test data.
func ConfigAndLoadTestData(config *config.Config, logFormat string) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(config, logFormat, 0)
	defer func() {
		err := app.DB.Close()
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Problem with closing database connection")
	}()

	// Drop all prior to create test data
	err := db.DropAllTables(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Migrate Database to the latest version
	err = db.MigrateDatabase(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Create Test DATA
	err = db.CreateTestData(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}
}

func ConfigAndMigrateDb(config *config.Config, logFormat string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(config, logFormat, 0)
	defer func() {
		err := app.DB.Close()
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Problem with closing database connection")
	}()

	// Migrate Database to the latest version
	return db.MigrateDatabase(ctx, app.DB, app.Logger)
}

// Initialize the app with config
func (app *App) Initialize(config *config.Config, logFormat string, wait int) {
	// Configuration
	app.Config = config

	// Configure Logger
	app.setLogger(config, logFormat)

	// Configure Sentry
	app.setSentry(config)

	// Configure DB
	myDb := app.connectToDB(config)
	app.BooksRepo = model.NewPostgresBookRepository(myDb)
	app.DB = myDb

	app.Routes = &routes.Config{
		Handler: handler.Controller{
			Logger:    app.Logger,
			Config:    app.Config,
			BooksRepo: app.BooksRepo,
		},
	}

	app.Routes.Routes()
}

func (app *App) setLogger(config *config.Config, format string) {
	app.Logger = logrus.New()
	app.Logger.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		app.Logger.SetFormatter(&logrus.JSONFormatter{})
		//app.Logger.SetReportCaller(true)
	} else {
		app.Logger.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	lvl, err := logrus.ParseLevel(config.Logger.Level)
	if err != nil {
		lvl = logrus.WarnLevel
		app.Logger.SetLevel(lvl)
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Configuration Error")
	} else {
		app.Logger.SetLevel(lvl)
	}
}

func (app *App) setSentry(config *config.Config) {
	err := sentry.Init(sentry.ClientOptions{
		// Either set your DSN here or set the SENTRY_DSN environment variable.
		Dsn: config.Sentry.Dsn,
		// Enable printing of SDK debug messages.
		// Useful when getting started or trying to figure something out.
		Debug: true,
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			// Here you can inspect/modify non-transaction events (for example, errors) before they are sent.
			// Returning nil drops the event.
			log.Printf("BeforeSend event [%s]", event.EventID)
			return event
		},
		BeforeSendTransaction: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			// Here you can inspect/modify transaction events before they are sent.
			// Returning nil drops the event.
			if strings.Contains(event.Message, "test-transaction") {
				// Drop the transaction
				return nil
			}
			event.Message += " [example]"
			log.Printf("BeforeSendTransaction event [%s]", event.EventID)
			return event
		},
		// Enable tracing
		EnableTracing: true,
		// Specify either a TracesSampleRate...
		TracesSampleRate: 1.0,
		// ... or a TracesSampler
		TracesSampler: sentry.TracesSampler(func(ctx sentry.SamplingContext) float64 {
			// Don't sample health checks.
			if ctx.Span.Name == "GET /v1/health" {
				return 0.0
			}

			return 1.0
		}),
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
}

func (app *App) connectToDB(c *config.Config) *sql.DB {
	// connect to postgres
	var counts int64
	for {
		connection, err := openDB(c.Postgres.Url)
		if err != nil {
			log.Println("Postgres not ready...")
			counts++
		} else {
			log.Println("Connected to database!")
			return connection
		}

		if counts > 10 {
			log.Println(err)
			return nil
		}

		log.Println("Backing off for two seconds...")
		time.Sleep(2 * time.Second)
		continue
	}

}

func openDB(url string) (*sql.DB, error) {
	myDb, err := sql.Open("pgx", url)
	if err != nil {
		return nil, err
	}

	err = myDb.Ping()
	if err != nil {
		return nil, err
	}

	return myDb, nil
}

// Run will start the http server on host that you pass in. host:<ip:port>
func (app *App) Run(host string, corsEnabled bool) {
	// Flush buffered events before the program terminates.
	// Set the timeout to the maximum duration the program can afford to wait.
	defer sentry.Flush(2 * time.Second)

	// use signals for shutdown server gracefully.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	go func() {
		if corsEnabled {
			corsInstance := cors.New(cors.Options{
				AllowedOrigins:   []string{"https://*", "http://*"},
				AllowedMethods:   []string{"GET", "POST", "PATCH", "PUT", "DELETE"},
				AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
				AllowCredentials: true,
				Debug:            false,
			})
			appHandler := corsInstance.Handler(app.Routes.Router)
			app.Logger.Fatal(http.ListenAndServe(host, appHandler))
		} else {
			app.Logger.Fatal(http.ListenAndServe(host, app.Routes.Router))
		}
	}()
	app.Logger.Infof("Server is listning on %s://%s", "http", host)

	sig := <-sigs
	app.Logger.Infoln("Signal: ", sig)

	app.Logger.Infoln("Stopping MongoDB Connection...")
	defer func() {
		err := app.DB.Close()
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Problem with closing database connection")
	}()
}
