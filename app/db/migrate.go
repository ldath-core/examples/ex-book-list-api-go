package db

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
)

func MigrateDatabase(ctx context.Context, conn *sql.DB, logger *logrus.Logger) error {
	err := migrateTables(ctx, conn, logger)
	if err != nil {
		logger.Error(err)
		return err
	}
	err = migrateIndexes(ctx, conn, logger)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Migration completed")
	return nil
}

func migrateTables(ctx context.Context, conn *sql.DB, logger *logrus.Logger) error {
	_, err := conn.ExecContext(ctx, "CREATE TABLE IF NOT EXISTS public.books (id integer NOT NULL, title character varying, author character varying, year character varying, description character varying, img_type character varying, img_data character varying)")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.ExecContext(ctx, "CREATE SEQUENCE IF NOT EXISTS public.books_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.ExecContext(ctx, "ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.ExecContext(ctx, "ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);")
	if err != nil {
		logger.Error(err)
		return err
	}
	return nil
}

func migrateIndexes(ctx context.Context, database *sql.DB, logger *logrus.Logger) error {
	return nil
}
