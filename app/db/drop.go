package db

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
)

func DropAllTables(ctx context.Context, conn *sql.DB, logger *logrus.Logger) error {
	_, err := conn.ExecContext(ctx, "DROP TABLE IF EXISTS public.books")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.ExecContext(ctx, "DROP SEQUENCE IF EXISTS public.books_id_seq")
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Dropping all tables completed")
	return nil
}
