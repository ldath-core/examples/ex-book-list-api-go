package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-list/app/handler"
)

type Config struct {
	Handler handler.Controller
	Router  *mux.Router
}

func (rc *Config) Routes() {

	rc.Router = mux.NewRouter()
	rc.Router.Use(handler.SentryMiddleware)
	rc.Router.Use(handler.JSONContentTypeMiddleware)

	rc.handleRoute("/v1/health", "GET", rc.Handler.GetHealth)
	rc.handleRoute("/v1/error", "GET", rc.Handler.GetError)
	rc.handleRoute("/v1/panic", "GET", rc.Handler.GetPanic)
	rc.handleRoute("/v1/books", "POST", rc.Handler.CreateBook)
	rc.handleRoute("/v1/books/{id}", "PATCH", rc.Handler.UpdateBook)
	rc.handleRoute("/v1/books/{id}", "PUT", rc.Handler.UpdateBook)
	rc.handleRoute("/v1/books/{id}", "GET", rc.Handler.GetBook)
	rc.handleRoute("/v1/books/{id}", "DELETE", rc.Handler.DeleteBook)
	rc.handleRoute("/v1/books", "GET", rc.Handler.GetBooks)
	rc.handleRoute("/v1/books", "GET", rc.Handler.GetBooks, "page", "{page}")
	rc.Router.NotFoundHandler = http.HandlerFunc(rc.Handler.NotFound)
}

func (rc *Config) handleRoute(path string, method string, endpoint http.HandlerFunc, queries ...string) {
	rc.Router.HandleFunc(path, endpoint).Methods(method).Queries(queries...)
}
