# Find GO environment path
GO_PATH=`go env GOPATH`

# Define NEWLINE required to compute string
NEWLINE=$'\n'
# Define separator
SEPARATOR="************************************"

function run_staticcheck()
{
    # Check if staticcheck is installed
    if ! [ -x "$(command  -v $GO_PATH/bin/staticcheck)" ]; then
        installation_url="   https://staticcheck.io/docs/getting-started/"
        return "staticcheck command cannnot be found. Please check how to install staticcheck:${installation_url}"
    fi

    output_prefix="${SEPARATOR}${NEWLINE}Executing process of static analysis:${NEWLINE}${SEPARATOR}"

    # Execute static check analysis on the project using staticcheck
    return_output=$($GO_PATH/bin/staticcheck -tests=false ./... 2>&1 >/dev/null )

    return_value=$?

    if [ $return_value -eq 0 ]; then
        return_output="    STATUS - OK"
    fi

    # Concatenate output response from scan
    echo "${output_prefix}${NEWLINE}${return_output}${NEWLINE} ${NEWLINE}"
}

function run_codestyle()
{
     # Check if gocheckstyle is installed
    if ! [ -x "$(command  -v $GO_PATH/bin/gocheckstyle)" ]; then
        installation_url="   https://github.com/qiniu/checkstyle#install"
        return "gocheckstyle command cannnot be found. Please check how to install gocheckstyle:${installation_url}"
    fi

    output_prefix="${SEPARATOR}${NEWLINE}Executing process of code style analysis:${NEWLINE}${SEPARATOR}"

    # Execute static check analysis on the project using staticcheck
    return_output=$( $GO_PATH/bin/gocheckstyle -config=.go_style app/  2>&1 >/dev/null )

    return_value=$?

    if [ $return_value -eq 0 ]; then
        return_output="    STATUS - OK"
    fi
    echo "${output_prefix}${NEWLINE}${return_output}${NEWLINE} ${NEWLINE}"
}